import React, { useState } from 'react';
import Autosuggest from 'react-autosuggest';
import '../Styles/AutocompleteSelect.css'; 
import { BsBuildingFillUp } from 'react-icons/bs';

const AutocompleteSelect = (props) => {
  const [value, setValue] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  const newOptions = [];

  for (let i = 1; i <= 50; i++) {
    newOptions.push(`${i}`);
  }
  const options = newOptions // Opciones de autocompletado

  const getSuggestions = (inputValue) => {
    return options.filter((option) =>
      option.toLowerCase().includes(inputValue.toLowerCase())
    );
  };

  const handleChange = (event, { newValue }) => {
    setValue(newValue);
    props.handleChange(newValue)
  };

  const handleSuggestionsFetchRequested = ({ value }) => {
    setSuggestions(getSuggestions(value));
  };

  const handleSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  

  const renderSuggestion = (suggestion) => (
    <div>
      {suggestion}
    </div>
  );

  const inputProps = {
    value,
    onChange: handleChange,
    type: "number",

  };

  const renderInputComponent = inputProps => (
    <div>
        <div>
          <label htmlFor="name">Introduzca Piso</label>
          <div className="input-container">
            <label htmlFor="name" className="label-icon">
              <BsBuildingFillUp className="icon" size={"2em"} />
            </label>
            <input className={`input ${props.isValid ? '' : 'invalid'}`}  placeholder={props.placeholder} {...inputProps} />
            {!props.isValid && <p className="error-message">{props.messageError}</p>}
          </div>
        </div>
    </div>
  );

  return (
    <Autosuggest
      suggestions={suggestions}
      onSuggestionsFetchRequested={handleSuggestionsFetchRequested}
      onSuggestionsClearRequested={handleSuggestionsClearRequested}
      getSuggestionValue={(suggestion) => suggestion}
      renderSuggestion={renderSuggestion}
      renderInputComponent={renderInputComponent}
      inputProps={inputProps}
    />
  );
};

export default AutocompleteSelect;