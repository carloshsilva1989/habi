import React, { useState } from 'react';
import '../Styles/Layout.css';
import Header from './Header'
import { MdOutlineSummarize } from 'react-icons/md';
import Modal from './Modal';
import { useSelector } from 'react-redux'
import  stepsData from '../Data/stepsData'

const Layout = ({ children }) => {
    const dataStep = useSelector((state) => state.dataStep)
    const [isOpen, setIsOpen] = useState(false);

    const openModal = () => {
      setIsOpen(true);
    };
  
    const closeModal = () => {
      setIsOpen(false);
    };

    const sideSummary = () => {
        if(dataStep.step < 6)
        {
            return <div className='div-summary'>
                <h1 className='title-primary-summary'>{"HABI PROGRESO"}</h1>
                <div style={{ marginTop: '30px' }}></div>
                <hr className="solid"></hr>
                <p style={{ color: "#777", fontSize: "17px", lineHeight: "1.5em", marginTop: "12px" }}>Estos datos son los más importantes para darte una oferta certera. Procura que la información que ingresas sea la real.</p>
                {summaryContent(dataStep, stepsData)}
                </div>
        }
    }

    const summaryContent = (dataStep, stepsData) => {
        let StepsSum = []
        stepsData.forEach(step => {
            let sliceValue= `${dataStep[step.value]}`
       
            StepsSum.push(
                <div key={step.label} className={`content-summary ${dataStep.step === step.order  ? 'active' : ''}`}>
                    <h1 className={`title-secondary-summary ${dataStep.step === step.order  ? 'active' : ''}`}>{step.label}</h1>
                    <p>{sliceValue}</p>
                </div>
            )
        });
        return StepsSum;
    }

    return (
        <div className='layout'>
            <div className='div-content'>
                <Header></Header>
                <div className="container">
                    {children}
                </div>
            </div>
            {sideSummary()}
            {dataStep.step < 6 &&
                <center>
                    <div style={{ textAlign: 'center' }}>
                        <button className={"fixedButton"} onClick={() => openModal()}>
                        <MdOutlineSummarize style={{position:'absolute',float:'left',marginLeft:'5px', marginTop:-4}} size={"2em"} />
                            Ver resumen
                        </button>
                    </div>
                </center>
            }

            {isOpen && (
                <Modal
                title="Resumen"
                content={summaryContent(dataStep, stepsData)}
                onClose={closeModal}
                />
            )}
        </div>
    );
};

export default Layout;