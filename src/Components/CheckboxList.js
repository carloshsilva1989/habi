import React, { useState } from 'react';

const CheckboxList = ({ values, onChange }) => {
  const [selectedValues, setSelectedValues] = useState([]);

  const handleCheckboxChange = (value) => {
    const updatedValues = selectedValues.includes(value)
      ? selectedValues.filter((val) => val !== value)
      : [...selectedValues, value];

    setSelectedValues(updatedValues);
    onChange(updatedValues);
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '8px' }}>
      {values.map((value) => (
        <label key={value} style={{ display: 'flex', alignItems: 'center', marginLeft: '5px' }}>
          <input
            type="checkbox"
            value={value}
            checked={selectedValues.includes(value)}
            onChange={() => handleCheckboxChange(value)}
            style={{ marginRight: '5px', transform: 'scale(1.7)', borderRadius: '5px' }}
          />
          <span style={{ marginLeft: '5px' }}>{value}</span>
        </label>
      ))}
    </div>
  );
};

export default CheckboxList;