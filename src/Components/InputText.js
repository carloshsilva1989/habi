import React from 'react';

const InputText = (props) => {

    return (
        <div>
          <label htmlFor="input">{props.title}</label>
          <div className="input-container">
            <label htmlFor="input" className="label-icon">
              {props.icon}
            </label>
            <div>
              <input
                onChange={props.onChange}
                type="text"
                className={`input-text ${props.isValid ? '' : 'invalid'}`}
                placeholder={props.placeholder}
              />
              {!props.isValid && <p className="error-message">{props.messageError}</p>}
            </div>
          </div>
        </div>
    );
};

export default InputText;

