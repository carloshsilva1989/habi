import React from 'react';

const Content = (props) => {
    return (
        <div>
            <h3 style={{ color: "#7C3AED" }}>{`Paso ${props.stepData.currentStep.order} de 6: ${props.stepData.currentStep.label}`}</h3>
            <p style={{fontFamily: 'Poppins', fontSize: "1.7rem", fontWeight: 500, lineHeight: "2.25rem" }}>{props.stepData.currentStep.description}</p>
            <hr></hr>
            <div className='form'>
                {props.children}
            </div>
        </div>
    );
};

export default Content;
