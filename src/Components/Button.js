import React from 'react';
import { BsArrowRight } from 'react-icons/bs';

const ButtonNext = (props) => {
    return (
        <div style={{ textAlign: 'center' }}>
            <button 
            className={`buttonNext ${props.isValid ? '' : 'inactiveButton'}`}
            onClick={props.onClick}>
                {props.text}
                <BsArrowRight style={{float:'right',marginTop: "-2px"}} size={"2em"} />
            </button>
        </div>
    );
};

export default ButtonNext;