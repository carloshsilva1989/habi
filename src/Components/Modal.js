import React from 'react';
import '../Styles/Modal.css';
import { TfiClose } from 'react-icons/tfi';

const Modal = ({ title, content, onClose }) => {
  return (
    <div className="modal-overlay">
      <div className="modal">
        <div className="modal-header">
          <h2>{title}</h2>
          <button className="modal-close" onClick={onClose}>
            <TfiClose className="icon" size={"3em"} color={"#fff"}/>
          </button>
        </div>
        <div className="modal-content">{content}</div>
      </div>
    </div>
  );
};

export default Modal;