import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux'

const ProgressBar = () => {
  const { step } = useSelector((state) => state.dataStep)
  const value = step
  const maxValue = 6;
  const activeColor = '#9733FF';
  const inactiveColor = '#EBEBEB';
  const progressBarRef = useRef(null);

  useEffect(() => {
    const progressBar = progressBarRef.current;
    progressBar.style.width = `${(value / maxValue) * 100}%`;
  }, [step]);

  return (
    <div
      style={{
        width: '100%',
        backgroundColor: inactiveColor,
        height: '12px',
        overflow: 'hidden',
      }}
    >
      <div
        ref={progressBarRef}
        style={{
          width: 0,
          height: '100%',
          backgroundColor: activeColor,
          transition: 'width 0.5s ease',
        }}
      />
    </div>
  );
};

export default ProgressBar;