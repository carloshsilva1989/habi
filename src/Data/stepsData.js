import React from "react"
import StepFive from "../Screen/Steps/StepFive"
import StepFour from "../Screen/Steps/StepFour"
import StepOne from "../Screen/Steps/StepOne"
import StepThree from "../Screen/Steps/StepThree"
import StepTwo from "../Screen/Steps/StepTwo"
import Summary from "../Screen/Steps/Summary"


const stepsData =
[
  {
    "component": <StepOne />,
    "path": "/datos-cliente",
    "order": 1,
    "description": "Datos de cliente",
    "label": "Datos de cliente",
    "messageError": "Por favor, introduzca su nombre. Solo permitido letras y espacio.",
    "value":"name"
  },
  {
    "component": <StepTwo />,
    "path": "/correo-cliente",
    "order": 2,
    "description": "Correo",
    "label": "Correo",
    "messageError": "Por favor, introduzca un correo electrónico válido.",
    "value":"email"
  },
  {
    "component": <StepThree />,
    "path": "/direccion-apartamento",
    "order": 3,
    "description": "Dirección del apartamento",
    "label": "Dirección del apartamento",
    "messageError": "Por favor, introduzca una dirección.",
    "value":"direction"
  },
  {
    "component": <StepFour />,
    "path": "/piso-apartamento",
    "order": 4,
    "description": "Piso apartamento",
    "label": "Piso apartamento",
    "messageError": "Por favor, introduzca un número de apartamento, máximo hasta el 50.",
    "value":"floor"
  },
  {
    "component": <StepFive />,
    "path": "/opciones-apartamento",
    "order": 5,
    "description": "Opciones ( BBQ, Parques de juegos, Salón comunal)",
    "label": "Opciones",
    "messageError": "",
    "value":"options"
  },
  {
    "component": <Summary />,
    "path": "/resumen",
    "order": 6,
    "description": "Resumen",
    "label": "Resumen",
    "messageError": "",
    "value":"summary"
  }
]

export default stepsData