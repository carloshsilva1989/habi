import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    step: 1,
    name: '',
    email: '',
    direction: '',
    floor: '',
    options: [],
    summary: ''
};

export const stepsSlice = createSlice({
    name: 'steps',
    initialState,
    reducers: {
        setStepSlice: (state, action) => {
            state.step = action.payload
        },
        setNameSlice: (state, action) => {
            state.name = action.payload
        },
        setEmailSlice: (state, action) => {
            state.email = action.payload
        },
        setDirectionSlice: (state, action) => {
            state.direction = action.payload
        },
        setFloorSlice: (state, action) => {
            state.floor = action.payload
        },
        setOptionsSlice: (state, action) => {
            state.options = action.payload
        },
        reset: () =>  initialState
    },
})

export const { setStepSlice, setNameSlice, setEmailSlice, setDirectionSlice, setFloorSlice, setOptionsSlice, reset } = stepsSlice.actions