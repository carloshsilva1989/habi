import { stepsSlice } from './slices/StepsSlice'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist'
import { createStore } from 'redux'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    dataStep: stepsSlice.reducer
})

const persistConfig = {
  key: 'user',
  storage: AsyncStorage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = createStore(persistedReducer)
export const persistor = persistStore(store)