import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import stepsData from './Data/stepsData';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Navigate to={stepsData[0].path} replace />} />
        {stepsData?.map((step => { return <Route key={step.order} path={step.path} element={step.component} /> }))}
      </Routes>
    </Router>
  );
}

export default App;
