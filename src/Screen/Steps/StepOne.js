import React, { useEffect, useState } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import { BiUserCircle } from 'react-icons/bi';
import Content from '../../Components/Content';
import ButtonNext from '../../Components/Button';
import { useNavigate } from 'react-router-dom';
import InputText from '../../Components/InputText'
import { useDispatch } from 'react-redux'
import { setNameSlice, setStepSlice } from '../../Store/slices/StepsSlice';

function StepOne() {
  const dispatch = useDispatch();
  const stepData = getSteps(1)
  const navigate = useNavigate();
  const [name, setName] = useState('');
  const [isValid, setIsValid] = useState(true);
  const [activeButton, setActiveButton] = useState(false);

  
  const handleClick = () => {
    if(name === '')
    {
      setIsValid(false)
      return;
    }

    dispatch(setNameSlice(name))
    navigate(stepData.nextStep.path);
  };

  const handleChange = (event) => {
    setIsValid(true)
    setActiveButton(true)
    const inputValue = event.target.value;
    if(!validateValue(inputValue))
    {
      setIsValid(false)
      setActiveButton(false)
    }
    setName(inputValue);
  };

  const validateValue = (value) => {
    const regex = /^[a-zA-Z\s]+$/;
    return regex.test(value);
  };

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])


  return (
    <Layout>
      <Content stepData={stepData}>
        <InputText
          title={"Introduzca nombre"}
          messageError={stepData.currentStep.messageError}
          placeholder={stepData.currentStep.label}
          icon={<BiUserCircle className="icon" size={"2em"} />}
          onChange={handleChange}
          isValid={isValid}
        />
        <ButtonNext isValid={activeButton} text={stepData.nextStep.label} onClick={isValid ? handleClick : console.log("Button block")} />
      </Content>
    </Layout>
  );
}

export default StepOne;