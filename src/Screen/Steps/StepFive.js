import React, { useEffect, useState } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import Content from '../../Components/Content';
import ButtonNext from '../../Components/Button';
import { useNavigate } from "react-router-dom";
import CheckboxList from '../../Components/CheckboxList'
import { useDispatch } from 'react-redux'
import { setOptionsSlice, setStepSlice } from '../../Store/slices/StepsSlice';

function StepFive() {
  const dispatch = useDispatch();
  const stepData = getSteps(5)
  const navigate = useNavigate();
  const [options, setOptions] = useState([]);

  const handleClick = () => {
    console.log('Button clicked!');
    dispatch(setStepSlice(5))
    dispatch(setOptionsSlice(options))
    navigate(stepData.nextStep.path);
  };

  const handleCheckboxChange = (selectedValues) => {
    console.log('Valores seleccionados:', selectedValues);
    setOptions(selectedValues)
  };

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])

  return (
    <Layout>
      <Content stepData={stepData}>
        <div>
          <label htmlFor="name">Seleccione una o varias (Opcional)</label>
          <div style={{paddingTop:'10px'}}></div>
          <div className="input-container">
            <CheckboxList values={['Zona BBQ', 'Salón comunal,', 'Parque de juegos']} onChange={handleCheckboxChange} />
          </div>
        </div>
        <ButtonNext isValid={true} text={stepData.nextStep.label} onClick={handleClick} />
      </Content>
    </Layout>
  );
}

export default StepFive;