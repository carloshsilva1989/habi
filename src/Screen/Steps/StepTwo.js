import React, { useEffect, useState } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import { HiOutlineMail } from 'react-icons/hi';
import Content from '../../Components/Content';
import ButtonNext from '../../Components/Button';
import { useNavigate } from "react-router-dom";
import InputText from '../../Components/InputText';
import { useDispatch, useSelector } from 'react-redux'
import { setEmailSlice, setStepSlice } from '../../Store/slices/StepsSlice';


function StepTwo() {
  const dispatch = useDispatch();
  const { step } = useSelector((state) => state.dataStep)
  const stepData = getSteps(2)
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [isValid, setIsValid] = useState(true);
  const [activeButton, setActiveButton] = useState(false);

  const handleClick = () => {
    if (email == '') {
      setIsValid(false)
      return
    }
    dispatch(setEmailSlice(email))
    navigate(stepData.nextStep.path);
  };

  const handleChange = (event) => {

    const inputValue = event.target.value;
    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    if (!emailRegex.test(inputValue)) {
      setIsValid(emailRegex.test(inputValue));
      setEmail('');
      setActiveButton(false)
      return;
    }
    setIsValid(true);
    setActiveButton(true)
    setEmail(inputValue);
  };

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])

  return (
    <Layout>
      <Content stepData={stepData}>
        <InputText
          title={"Introduzca Correo"}
          messageError={stepData.currentStep.messageError}
          placeholder={stepData.currentStep.label}
          icon={<HiOutlineMail className="icon" size={"2em"} />}
          onChange={handleChange}
          isValid={isValid}
        />
        <ButtonNext isValid={activeButton} text={stepData.nextStep.label} onClick={isValid ? handleClick : console.log("Button block")} />
      </Content>
    </Layout>
  );
}

export default StepTwo;