import React, { useEffect } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import Content from '../../Components/Content';
import { useDispatch, useSelector } from 'react-redux'
import { setStepSlice, reset } from '../../Store/slices/StepsSlice';
import  stepsData from '../../Data/stepsData'
import ButtonNext from '../../Components/Button';
import { useNavigate } from 'react-router-dom';


function Summary() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const dataStep = useSelector((state) => state.dataStep)
  const stepData =  getSteps(6);
  const stepDataFiltered = stepsData.filter(step => step.value !== 'summary')

  const summaryContent = (dataStep, stepsData) => {
    let StepsSum = []
    stepsData.forEach(step => {
        let sliceValue= `${dataStep[step.value]}`
      
        StepsSum.push(
            <div>
                <h1 className={`title-secondary-summary active`}>{step.label}</h1>
                <p>{sliceValue}</p>
            </div>
        )
    });
    return StepsSum;
  }

  const handleClick = () => {
    navigate('/')
    dispatch(reset())
  }

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])

  return (
    <Layout>
      <Content stepData={stepData}>
        {summaryContent(dataStep, stepDataFiltered)}
        <ButtonNext isValid={true} text={"Finalizar"} onClick={() => handleClick()} />
      </Content>
    </Layout>
  );
}

export default Summary;