import React, { useEffect, useState } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import { TbMapSearch } from 'react-icons/tb';
import Content from '../../Components/Content';
import ButtonNext from '../../Components/Button';
import { useNavigate } from "react-router-dom";
import InputText from '../../Components/InputText';
import { useDispatch } from 'react-redux'
import { setDirectionSlice, setStepSlice } from '../../Store/slices/StepsSlice';

function StepThree() {
  const dispatch = useDispatch();
  const stepData = getSteps(3)
  const navigate = useNavigate();
  const [direction, setDirection] = useState('');
  const [isValid, setIsValid] = useState(true);
  const [activeButton, setActiveButton] = useState(false);

  const handleClick = () => {
    if(!validarValor(direction))
    {
      setIsValid(false)
      return;
    }

    dispatch(setDirectionSlice(direction))
    navigate(stepData.nextStep.path);
  };

  function validarValor(valor) {
    if (valor === undefined || valor === null) {
      return false;
    }
  
    if (typeof valor === 'string') {
      valor = valor.trim(); 
    }
  
    return valor.length !== 0;
  }
  

  const handleChange = (event) => {
    setIsValid(true)
    const inputValue = event.target.value;
    console.log(inputValue)
    if (!validarValor(inputValue)) {
      setIsValid(false)
      setActiveButton(false)
      return;
    }
    setIsValid(true)
    setActiveButton(true)
    setDirection(inputValue);
  };

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])

  return (
    <Layout>
      <Content stepData={stepData}>
        <InputText
          title={"Introduzca Dirección de apartamento"}
          messageError={stepData.currentStep.messageError}
          placeholder={stepData.currentStep.label}
          icon={<TbMapSearch className="icon" size={"2em"} />}
          onChange={handleChange}
          isValid={isValid}
        />
        <ButtonNext isValid={activeButton} text={stepData.nextStep.label} onClick={isValid ? handleClick : console.log("Button block")} />
      </Content>
    </Layout>
  );
}

export default StepThree;