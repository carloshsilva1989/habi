import React, { useEffect, useState } from 'react';
import { getSteps } from '../../Utils/index'
import Layout from '../../Components/Layout';
import Content from '../../Components/Content';
import ButtonNext from '../../Components/Button';
import { useNavigate } from "react-router-dom";
import AutocompleteSelect from '../../Components/AutoCompleteSelect.js';
import { useDispatch } from 'react-redux'
import { setFloorSlice, setStepSlice } from '../../Store/slices/StepsSlice';


function StepFour() {
  const dispatch = useDispatch();
  const stepData = getSteps(4)
  const navigate = useNavigate();
  const [floor, setFloor] = useState('');
  const [isValid, setIsValid] = useState(true);
  const [activeButton, setActiveButton] = useState(false);

  const handleChange = (event) => {
    setIsValid(true)
    const inputValue = event;
    if(inputValue > 50)
    {
      setIsValid(false)
      return
    }
    setFloor(inputValue);
    setActiveButton(true);
  };

  const handleClick = () => {
    if(floor === '')
    {
      setIsValid(false)
      return;
    }
    dispatch(setFloorSlice(floor))
    navigate(stepData.nextStep.path);
  };

  useEffect(() => {
    dispatch(setStepSlice(stepData.currentStep.order))
  }, [])
  
  return (
    <Layout>
      <Content stepData={stepData}>
        <AutocompleteSelect messageError={stepData.currentStep.messageError} isValid={isValid} handleChange={handleChange}/>
        <ButtonNext isValid={activeButton} text={stepData.nextStep.label} onClick={isValid ? handleClick : console.log("Button block")} />
      </Content>
    </Layout>
  );
}

export default StepFour;