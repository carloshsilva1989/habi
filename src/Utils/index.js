import stepsData from '../Data/stepsData'

export const getSteps = (step) => {
    const { currentStep, nextStep } = stepsData.reduce((acc, curr) => {
      if (curr.order === step) {
        acc.currentStep = curr;
      }
      if (curr.order === step + 1) {
        acc.nextStep = curr
      }
      return acc;
    }, {})
    return { currentStep, nextStep };
}

export const getCountSteps = () => {
  let num = 1
  stepsData.map((item) => {
    num++
  })
  return num
}