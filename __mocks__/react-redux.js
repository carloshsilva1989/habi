const actualReactRedux = jest.requireActual('react-redux');
const mockDispatch = jest.fn((action) => action);
const initialState = {
    step: 1,
    name: '',
    email: '',
    direction: '',
    floor: '',
    options: [],
    summary: ''
};

module.exports = {
    ...actualReactRedux,
    useSelector: jest.fn().mockImplementation(() => initialState),
    useDispatch: jest.fn().mockImplementation(() => mockDispatch),
    connect: (mapStateToProps, mapDispatchToProps) => (reactComponent) => ({
        mapStateToProps,
        mapDispatchToProps: (ownProps, dispatch = mockDispatch) => mapDispatchToProps(dispatch, ownProps),
        reactComponent,
        mockDispatch,
    }),
    Provider: ({ children }) => children,
};
