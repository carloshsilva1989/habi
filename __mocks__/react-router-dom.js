
const actualReactRouterDom = jest.requireActual('react-router-dom');
const mockedUsedNavigate = jest.fn();

module.exports = {
    ...actualReactRouterDom,
    useNavigate: () => mockedUsedNavigate
};