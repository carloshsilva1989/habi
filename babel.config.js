module.exports = {
    presets:[
        "@babel/preset-env",
        "@babel/preset-react"
    ],
    plugins: [
        [
            'module-resolver',
            {
                extensions: ['.tsx', '.ts', '.js', '.json', '.css'],
            },
        ],
    ],
};