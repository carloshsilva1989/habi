import React from "react";
import { render, screen,  } from '@testing-library/react';
import StepThree from "../../../src/Screen/Steps/StepThree";

describe('Test suite screen StepThree', () => {
    test('Deberia renderizar correctamente', () => {
        render(<StepThree />)
        expect(screen.getByText('Introduzca Dirección de apartamento')).toBeDefined();
    });
});