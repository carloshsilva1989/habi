import React from "react";
import { render, screen,  } from '@testing-library/react';
import StepFive from "../../../src/Screen/Steps/StepFive";

describe('Test suite screen StepFive', () => {
    test('Deberia renderizar correctamente', () => {
        render(<StepFive />)
        expect(screen.getByText('Seleccione una o varias (Opcional)')).toBeDefined();
    });
});