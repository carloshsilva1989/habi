import React from "react";
import { render, screen,  } from '@testing-library/react';
import StepFour from "../../../src/Screen/Steps/StepFour";
import { getSteps } from "../../../src/Utils";

describe('Test suite screen StepOne', () => {
    test('Deberia renderizar correctamente', () => {
        const { nextStep } = getSteps(4)
        render(<StepFour />)
        const elements = screen.getAllByText(nextStep.label) 
        expect(elements.length).toBe(2);
    });
});