import React from "react";
import { render, screen,  } from '@testing-library/react';
import StepTwo from "../../../src/Screen/Steps/StepTwo";

describe('Test suite screen StepTwo', () => {
    test('Deberia renderizar correctamente', () => {
        render(<StepTwo />)
        expect(screen.getByText('Introduzca Correo')).toBeDefined();
    });
});