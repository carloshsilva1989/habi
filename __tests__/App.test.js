import React from "react";
import { render, screen } from '@testing-library/react';
import App from '../src/App';

describe("Test Suite App", () => {
  it("Deberia renderizar correctamente", async () => {
    render(<App />);
    expect(screen).toBeDefined();
  });
});
